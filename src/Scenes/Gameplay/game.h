#pragma once
#include <iostream>
#include <string>
#include "..\..\RayLib\include\raylib.h"

using namespace std;

namespace pong {

    struct Flipper {
        Rectangle body;
        Color color = BLACK;
        string name;
        int keyUp = KEY_W;
        int keyDown = KEY_S;
        int keyPU = KEY_D;
    };
    struct Ball {
        Vector2 pos;
        int rad = 0;
        Color color = BLACK;
    };
    struct Limit {
        Rectangle body;
        Color color = BLACK; // Optional 
    };
    struct Limits {
        Limit up; //Horizontal Limit
        Limit down; //Vertical Limit
        Limit left; //Horizontal Limit
        Limit right; //Vertical Limit
    };
    struct Scores {
        int scoreP1 = 0;
        int scoreP2 = 0;
    };
    struct Modif {
        float speedVar = 1;
        int direction = 1; //positivo = derecha, negativo = izquierda
        bool winState = false;
        int objetive = 3;
        int keyPause = KEY_P;
        int keyQuit = KEY_F1;
    };

	namespace game {

        void init();
		void draw();
        void ballStartPos(Ball&);
        void flipperColCheck();
        void limitColCheck();
        void winCheck();
        void gameController();
        void resetGeneralOptions();

	}

}