#include <iostream>
#include <string>
#include "..\..\RayLib\include\raylib.h"
#include "Extern Vars/extern_var.h"
#include "Scenes/Options/options.h"
#include "Scenes/Menu/menus.h"
#include "game_start.h"

using namespace std;
using namespace config;

void game_init() {
    
    pong::game::init();

    InitWindow(screen::SCREEN_WIDTH, screen::SCREEN_HEIGHT, "CASI un PONG!");
    SetTargetFPS(50);  // Set our game to run at 50 frames-per-second

    do {

        using namespace pong;
        using namespace menu;

        init();
        update();
        draw();

        switch ((MENU)selectedMenuOption(pong::menu::menu)) {

            case MENU::PLAY:
                game::gameController();
                break;

            case MENU::OPT:
                do {

                    pong::options::init();
                    pong::options::draw();

                } while (!IsKeyPressed(KEY_F1));
                break;

            case MENU::EXIT:
                pre_config::quit =true;
                break;

            default:
                break;

        }

    } while (!pre_config::quit);
}

