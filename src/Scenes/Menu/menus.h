#pragma once
#include <iostream>
#include <string>
#include "..\..\RayLib\include\raylib.h"

using namespace std;

namespace pong {

    namespace menu {

        enum class MENU { PLAY, OPT, EXIT };
        const int MENU_Q = 3;

        static int hoveredOpt = -1;
        static int yPos[3];
        static int xPos;

        void init();
        void update();
        void draw();
        int selectedMenuOption(Rectangle[]);

    }

}