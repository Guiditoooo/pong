#include "options.h"
#include "Extern Vars/extern_var.h"

namespace pong {

    namespace options {

        int distance;
        int posXinit;
        int posYinit;

        void init() {

            distance = 200;
            posXinit = 120;
            posYinit = 50;

        }

        void draw() {

            //pong::options::init();

            using namespace config;
            using namespace flipper;

            BeginDrawing();
            ClearBackground(RAYWHITE);

            for (int i = 0; i < 4; i++) {

                if (i == 0) {

                    const char* texto = &flp1.name[0];
                    //Legend
                    DrawText("P1", posXinit + distance * i, posYinit, 40, BLACK);
                    //Color Pad
                    DrawRectangle(posXinit - (float)flp1.body.width, posXinit, (float)flp1.body.height, (float)flp1.body.width, flp1.color);
                    //Keys
                    DrawText("Arriba:", posXinit - posYinit, posYinit * 7 / 2, SIZE_TINY, BLACK);
                    DrawText(FormatText("%c", (char)flp1.keyUp), posXinit + 3 * posYinit / 2 - 10, posYinit * 7 / 2, SIZE_TINY, BLACK);
                    DrawText("Abajo:", posXinit - posYinit, posYinit * 9 / 2, SIZE_TINY, BLACK);
                    DrawText(FormatText("%c", (char)flp1.keyDown), posXinit + 3 * posYinit / 2 - 10, posYinit * 9 / 2, SIZE_TINY, BLACK);
                    /*DrawText("Poder:", posXinit - posYinit, posYinit * 11 / 2, 30, BLACK);
                    DrawText(FormatText("%c", (char)flp1.keyPU), posXinit + 3 * posYinit / 2 - 10, posYinit * 11 / 2, SIZE_TINY, BLACK);*/
                    //Name
                    DrawText(texto, posXinit - posYinit, posYinit * 13 / 2, SIZE_TINY, BLACK);

                }

                if (i == 1) {

                    const char* texto = &flp2.name[0];
                    //Legend
                    DrawText("P2", posXinit + distance * i, posYinit, 40, BLACK);
                    //Pad
                    DrawRectangle(posXinit - flp2.body.width + distance + 5, posXinit, flp2.body.height, flp2.body.width, flp2.color);
                    distance += 10;
                    //Keys
                    DrawText("Arriba:", posXinit - posYinit + distance, posYinit * 7 / 2, SIZE_TINY, BLACK);
                    DrawText(FormatText("%c", (char)flp2.keyUp), posXinit + 3 * posYinit / 2 + distance, posYinit * 7 / 2, SIZE_TINY, BLACK);
                    DrawText("Abajo:", posXinit - posYinit + distance, posYinit * 9 / 2, SIZE_TINY, BLACK);
                    DrawText(FormatText("%c", (char)flp2.keyDown), posXinit + 3 * posYinit / 2 + distance, posYinit * 9 / 2, SIZE_TINY, BLACK);
                    /*DrawText("Poder:", posXinit - posYinit + distance, posYinit * 11 / 2, 30, BLACK);
                    DrawText(FormatText("%c", (char)flp2.keyPU), posXinit + 3 * posYinit / 2 + distance, posYinit * 11 / 2, SIZE_TINY, BLACK);*/
                    //Name
                    DrawText(texto, posXinit - posYinit + distance, posYinit * 13 / 2, SIZE_TINY, BLACK);

                }

                if (i == 2) {

                    distance -= 20;
                    //Legend
                    DrawText("GENERAL", posXinit + distance * i, posYinit, 40, BLACK);
                    //Pad
                    distance += 35;
                    DrawCircle(posXinit + distance * i + 20, posYinit / 2 + posXinit - SIZE_TINY/2, SIZE_TINY/2, ball::ball.color);
                    //Keys
                    DrawText("Puntos:", posXinit - posYinit + distance * i, posYinit * 7 / 2, SIZE_TINY, BLACK);
                    DrawText(FormatText("%i", pre_config::mod.objetive), posXinit + 3 * posYinit / 2 + distance * i, posYinit * 7 / 2, SIZE_TINY, BLACK);
                    DrawText("Pausa:", posXinit - posYinit + distance * i, posYinit * 9 / 2, SIZE_TINY, BLACK);
                    DrawText(FormatText("%c", (char)pre_config::mod.keyPause), posXinit + 3 * posYinit / 2 + distance * i, posYinit * 9 / 2, SIZE_TINY, BLACK);
                    DrawText("Salir/Atras:", posXinit - posYinit + distance * i, posYinit * 11 / 2, SIZE_TINY, BLACK);
                    DrawText("F1", MeasureText("Salir/Atras:",SIZE_TINY) + posXinit - posYinit + distance * i + SIZE_TINY/2, posYinit * 11 / 2, SIZE_TINY, BLACK);

                }

                /*if (i == 3) {

                    DrawText("%", screen::SCREEN_WIDTH - posYinit, 10, 40, BLACK);

                }*/
            }

            EndDrawing();

        }  

    }

}