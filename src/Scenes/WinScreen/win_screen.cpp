#include "win_screen.h"
#include "Extern Vars/extern_var.h"

namespace pong {

    namespace win_screen {

        void pong::win_screen::winSign(Flipper pj) {

            while (!IsKeyDown(KEY_ENTER) && !WindowShouldClose() && !IsKeyDown(KEY_F1)) {

                pong::win_screen::draw(pj);

            }

        }
        
        void pong::win_screen::draw(Flipper pj) {

            using namespace config;

            BeginDrawing();

            ClearBackground(RAYWHITE);

            string aux = pj.name + "  WINS!";
            char text[100];
            strcpy_s(text, aux.c_str());

            DrawText(text, screen::SCREEN_WIDTH / 2 - MeasureText(text,SIZE_TINY)/2, screen::SCREEN_HEIGHT / 2 - SIZE_TINY, SIZE_TINY, pj.color);
            DrawText("Presiona ENTER para ir al menu!", screen::SCREEN_WIDTH / 2 - MeasureText("Presiona ENTER para ir al menu!", SIZE_TINY) / 2, screen::SCREEN_HEIGHT / 2 + SIZE_TINY, SIZE_TINY, pj.color);

            EndDrawing();

        }

    }

}