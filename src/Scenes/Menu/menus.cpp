#include "menus.h"
#include "Extern Vars/extern_var.h"

using namespace config;

namespace pong {

    namespace menu {

        Rectangle menu[pong::menu::MENU_Q];
        Rectangle opt[pong::options::OPT_Q];

        void pong::menu::init() {
            
            yPos[0] = screen::SCREEN_HEIGHT / 2 - 30;
            yPos[1] = screen::SCREEN_HEIGHT / 2 + 20;
            yPos[2] = screen::SCREEN_HEIGHT / 2 + 70;
            xPos = screen::SCREEN_WIDTH / 2 - 100;

            for (int i = 0; i < pong::menu::MENU_Q; i++) {

                opt[i].x = (float)(xPos + 25);
                opt[i].y = (float)(yPos[i] - 20);
                opt[i].height = 40;
                opt[i].width = 140;

                if (i == 1) {

                    pong::menu::menu[i].width += 110;

                }

                swap(menu[i], opt[i]);

            }
        }

        void pong::menu::update() {

            Vector2 mousePoint = GetMousePosition();

            for (int i = 0; i < pong::menu::MENU_Q; i++) {

                if (CheckCollisionPointRec(mousePoint, opt[i])) {

                    hoveredOpt = i;

                }

            }

        }

        void pong::menu::draw() {

            BeginDrawing();
            ClearBackground(RAYWHITE);

            DrawText("ALMOST PONG", screen::SCREEN_WIDTH/2-MeasureText("ALMOST PONG", SIZE_GIANT)/2, screen::SCREEN_HEIGHT / 2 - (SIZE_GIANT+SIZE_MEDIUM), SIZE_GIANT, RED); //alto es 75
            
            DrawText("PLAY", screen::SCREEN_WIDTH / 2 - SIZE_MEDIUM, screen::SCREEN_HEIGHT / 2 - SIZE_SMALL, SIZE_SMALL, BROWN); // alto es 50 aprox
            DrawText("RULES", screen::SCREEN_WIDTH / 2 - SIZE_MEDIUM, screen::SCREEN_HEIGHT / 2 - 5, SIZE_SMALL, BROWN);
            DrawText("QUIT", screen::SCREEN_WIDTH / 2 - SIZE_MEDIUM, screen::SCREEN_HEIGHT / 2 + 45, SIZE_SMALL, BROWN);

            switch (hoveredOpt) {

                case 0:
                    DrawCircle(xPos, yPos[0], 10, SKYBLUE);
                    break;

                case 1:
                    DrawCircle(xPos, yPos[1], 10, SKYBLUE);
                    break;

                case 2:
                    DrawCircle(xPos, yPos[2], 10, SKYBLUE);
                    break;

                default:
                    break;

            }

            DrawText("By: Guido Tello", SIZE_SMALL, screen::SCREEN_HEIGHT / 2 + SIZE_GIANT + SIZE_SMALL, SIZE_TINY, BROWN);

            DrawText("Lib Used: Raylib", screen::SCREEN_WIDTH - MeasureText("Lib Used: Raylib", SIZE_TINY) - SIZE_SMALL, screen::SCREEN_HEIGHT / 2 + SIZE_GIANT + SIZE_SMALL, SIZE_TINY, BROWN);

            EndDrawing();

        }

        int selectedMenuOption(Rectangle menu[]) {

            for (int i = 0; i < pong::menu::MENU_Q; i++) {

                if (IsMouseButtonDown(MOUSE_LEFT_BUTTON)) {

                    if (CheckCollisionPointRec(GetMousePosition(), menu[i])) {

                        return i;

                    }

                }

            }

            return -1;

        }

    }

}