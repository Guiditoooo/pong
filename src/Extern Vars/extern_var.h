#pragma once
#include "raylib.h"
#include "Scenes/Gameplay/game.h"
#include "Scenes/Menu/menus.h"
#include "Scenes/Options/options.h"

namespace pong {

	enum class STATE { MENU, OPTIONS, GAME, WIN };
	const int STATE_Q = 4;

	const int SIZE_GIANT = 100;
	const int SIZE_MEDIUM = 75;
	const int SIZE_SMALL = 55;
	const int SIZE_TINY = 30;

	namespace menu {

		extern Rectangle menu[pong::menu::MENU_Q];
		extern Rectangle opt[pong::options::OPT_Q];

	}
}

using namespace pong;

namespace config {

	namespace screen {

		const int SCREEN_WIDTH = 800;
		const int SCREEN_HEIGHT = 450;

	}

	namespace pre_config {

		const int POINT_LIMIT = 3; //a cuanto se juega
		const bool DEFAULT_PAUSE = true; //Empieza pausado

		extern Modif mod;
		extern Scores score;
		extern bool quit;
		extern bool pause;

	}

	namespace ball {

		const int BALL_SPEED = 3;
		const int BALL_RADIUS = 10;

		extern Ball ball;

	}
	namespace flipper {

		const int FLIPPER_WIDTH = 20;
		const int FLIPPER_HEIGHT = 80;
		const int FLIPPER_SPEED = 7;

		extern Flipper flp1;
		extern Flipper flp2;

	}

	namespace game_area {

		const int LIM_THICKNESS = 5;

		const int TOP_MARGIN = 35;
		const int DISTANCE_FROM_LOSE_TO_FLIPPER = flipper::FLIPPER_WIDTH / 2;

		const int GAME_AREA_WIDTH = screen::SCREEN_WIDTH - screen::SCREEN_WIDTH / 10;
		const int GAME_AREA_HEIGHT = screen::SCREEN_HEIGHT - screen::SCREEN_HEIGHT / 10;

		extern Limits lim;

	}

}