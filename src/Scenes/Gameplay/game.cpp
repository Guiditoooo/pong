#include "game.h"
#include "Extern Vars/extern_var.h"
#include "Scenes/WinScreen/win_screen.h"

using namespace config;

namespace pong {

    namespace game {

        Ball ball;

        void init() {

            //General
            resetGeneralOptions();
            //-------------------------------------------------------
            #pragma region Flippers
//Key Conf
            flipper::flp2.keyUp = KEY_W;
            flipper::flp2.keyDown = KEY_S;
            flipper::flp2.keyPU = KEY_D;
            flipper::flp1.keyUp = KEY_O;
            flipper::flp1.keyDown = KEY_L;
            flipper::flp1.keyPU = KEY_K;
            //Names
            flipper::flp1.name = "Player1";
            flipper::flp2.name = "Player2";
            //Dimentions
            flipper::flp1.body.width = flipper::FLIPPER_WIDTH;
            flipper::flp2.body.width = flipper::FLIPPER_WIDTH;
            flipper::flp1.body.height = flipper::FLIPPER_HEIGHT;
            flipper::flp2.body.height = flipper::FLIPPER_HEIGHT;
            //Color
            flipper::flp1.color = { 100,0,0,255 };
            flipper::flp2.color = { 0,100,0,230 };
            //x
            flipper::flp1.body.x = screen::SCREEN_WIDTH - game_area::GAME_AREA_WIDTH - flipper::flp1.body.width / 2;
            flipper::flp2.body.x = game_area::GAME_AREA_WIDTH - flipper::flp2.body.width / 2;
            //y
            flipper::flp1.body.y = game_area::GAME_AREA_HEIGHT / 2 - (screen::SCREEN_HEIGHT - game_area::GAME_AREA_HEIGHT) + game_area::TOP_MARGIN;
            flipper::flp2.body.y = flipper::flp1.body.y;
#pragma endregion
            //-------------------------------------------------------
            //Ball
            pong::game::ballStartPos(ball::ball);
            //-------------------------------------------------------
            #pragma region Horizontal Limits
//Color
            game_area::lim.up.color = BLACK;
            game_area::lim.down.color = BLACK;
            //Dims
            game_area::lim.up.body.height = game_area::LIM_THICKNESS;
            game_area::lim.down.body.height = game_area::LIM_THICKNESS;
            game_area::lim.up.body.width = game_area::GAME_AREA_WIDTH;
            game_area::lim.down.body.width = game_area::GAME_AREA_WIDTH;
            //y
            game_area::lim.up.body.y = game_area::TOP_MARGIN;
            game_area::lim.down.body.y = screen::SCREEN_HEIGHT - game_area::lim.up.body.height / 2 - (screen::SCREEN_HEIGHT - game_area::GAME_AREA_HEIGHT - game_area::TOP_MARGIN);
            //x
            game_area::lim.up.body.x = (screen::SCREEN_WIDTH - game_area::GAME_AREA_WIDTH) / 2;
            game_area::lim.down.body.x = game_area::lim.up.body.x;
#pragma endregion
            //-------------------------------------------------------
            #pragma region Vertical Limits
            //Color
            game_area::lim.left.color = PINK;
            game_area::lim.right.color = PINK;
            //Dims
            game_area::lim.left.body.height = game_area::GAME_AREA_HEIGHT;
            game_area::lim.left.body.width = (screen::SCREEN_WIDTH - game_area::GAME_AREA_WIDTH) / 2;
            game_area::lim.right.body.height = game_area::GAME_AREA_HEIGHT;
            game_area::lim.right.body.width = (screen::SCREEN_WIDTH - game_area::GAME_AREA_WIDTH) / 2;
            //y
            game_area::lim.left.body.y = game_area::TOP_MARGIN;
            game_area::lim.right.body.y = game_area::TOP_MARGIN;
            //x
            game_area::lim.left.body.x = flipper::flp1.body.x - game_area::DISTANCE_FROM_LOSE_TO_FLIPPER - flipper::flp1.body.width * 2;
            game_area::lim.right.body.x = flipper::flp2.body.x + game_area::DISTANCE_FROM_LOSE_TO_FLIPPER + flipper::flp2.body.width;
#pragma endregion

        }

        void draw() {

            BeginDrawing();

            ClearBackground(RAYWHITE);

            //Static
            DrawRectangle(game_area::lim.left.body.x, game_area::lim.left.body.y, game_area::lim.left.body.width, game_area::lim.left.body.height, game_area::lim.left.color);
            DrawRectangle(game_area::lim.right.body.x, game_area::lim.right.body.y, game_area::lim.right.body.width, game_area::lim.right.body.height, game_area::lim.right.color);
            DrawRectangle(game_area::lim.up.body.x, game_area::lim.up.body.y, game_area::lim.up.body.width, game_area::lim.up.body.height, game_area::lim.up.color);
            DrawRectangle(game_area::lim.down.body.x, game_area::lim.down.body.y, game_area::lim.down.body.width, game_area::lim.down.body.height, game_area::lim.down.color);

            //Mobile
            DrawRectangle(flipper::flp1.body.x, flipper::flp1.body.y, flipper::flp1.body.width, flipper::flp1.body.height, flipper::flp1.color);
            DrawRectangle(flipper::flp2.body.x, flipper::flp2.body.y, flipper::flp2.body.width, flipper::flp2.body.height, flipper::flp2.color);
            DrawCircleV(ball::ball.pos, ball::ball.rad, ball::ball.color);


            //Scoring
            string space = "                                ";
            string puntuacion = "       P1 -> " + to_string(pre_config::score.scoreP1) + space + space + "P2 -> " + to_string(pre_config::score.scoreP2);

            char aux[100];
            strcpy_s(aux, puntuacion.c_str());
            DrawText(aux, 10, 10, 20, RED);

            if (config::pre_config::pause) {
                DrawRectangle(screen::SCREEN_WIDTH / 2 - MeasureText("PAUSA", SIZE_SMALL) / 2, screen::SCREEN_HEIGHT / 2 - SIZE_SMALL, MeasureText("PAUSA", SIZE_SMALL), SIZE_SMALL, RAYWHITE);
                DrawText("PAUSA", screen::SCREEN_WIDTH / 2 - MeasureText("PAUSA", SIZE_SMALL) / 2, screen::SCREEN_HEIGHT / 2 - SIZE_SMALL,SIZE_SMALL,BLACK);

                DrawRectangle(screen::SCREEN_WIDTH / 2 - MeasureText("Presiona P para reanudar!", SIZE_TINY) / 2, screen::SCREEN_HEIGHT / 2 + SIZE_TINY, MeasureText("Presiona P para reanudar!", SIZE_TINY), SIZE_TINY, RAYWHITE);
                DrawText("Presiona P para reanudar!", screen::SCREEN_WIDTH / 2 - MeasureText("Presiona P para reanudar!", SIZE_TINY) / 2, screen::SCREEN_HEIGHT / 2 + SIZE_TINY, SIZE_TINY, BLACK);

                DrawRectangle(screen::SCREEN_WIDTH / 2 - MeasureText("Presiona F1 para volver al menu!", SIZE_TINY) / 2, screen::SCREEN_HEIGHT / 2 - SIZE_GIANT, MeasureText("Presiona F1 para volver al menu!", SIZE_TINY), SIZE_TINY, RAYWHITE);
                DrawText("Presiona F1 para volver al menu!", screen::SCREEN_WIDTH / 2 - MeasureText("Presiona F1 para volver al menu!", SIZE_TINY) / 2, screen::SCREEN_HEIGHT / 2 - SIZE_GIANT, SIZE_TINY, BLACK);

            }


            EndDrawing();
        }

        void ballStartPos(Ball& ball) {
            //Color
            ball.color = BLACK;
            //Radius
            ball.rad = ball::BALL_RADIUS;
            //Pos
            ball.pos = { (float)((screen::SCREEN_WIDTH / 2) - (ball.rad / 2)) , (float)((screen::SCREEN_HEIGHT / 2) + game_area::TOP_MARGIN / 2) };
        }

        void flipperColCheck() {
            if (CheckCollisionCircleRec(ball::ball.pos, ball::ball.rad, flipper::flp1.body) || CheckCollisionCircleRec(ball::ball.pos, ball::ball.rad, flipper::flp2.body)) {
                pre_config::mod.speedVar *= -1;
                pre_config::mod.speedVar *= 1.15f;
            }
        }

        void limitColCheck() {
            if (CheckCollisionCircleRec(ball::ball.pos, ball::ball.rad, game_area::lim.up.body) || CheckCollisionCircleRec(ball::ball.pos, ball::ball.rad, game_area::lim.down.body)) {
                pre_config::mod.direction *= -1;
            }
        }

        void winCheck() {

            using namespace pre_config;

            if (CheckCollisionCircleRec(ball::ball.pos, (float)ball::ball.rad, game_area::lim.left.body)) {
                game::ballStartPos(ball::ball);
                score.scoreP2++;
                mod.speedVar = 1;
                pause = DEFAULT_PAUSE;
            }
            if (CheckCollisionCircleRec(ball::ball.pos, (float)ball::ball.rad, game_area::lim.right.body)) {
                game::ballStartPos(ball::ball);
                score.scoreP1++;
                mod.speedVar = 1;
                pause = DEFAULT_PAUSE;
            }
            if (score.scoreP1 == mod.objetive) {
                pong::win_screen::winSign(flipper::flp1);
                mod.winState = true;
            }
            if (score.scoreP2 == mod.objetive) {
                pong::win_screen::winSign(flipper::flp2);
                mod.winState = true;
            }
        }

        void gameController() {

            using namespace config;
            using namespace pre_config;

            resetGeneralOptions();

            do {

                mod.winState = false;

                while (!mod.winState && !quit && !IsKeyPressed(KEY_F1)){

                    if (IsKeyPressed(mod.keyPause)) {
                        pause = !pause;
                    }

                    if (!pause) {

                        using namespace flipper;
                        using namespace game_area;
                        using namespace ball;

                        if (IsKeyDown(flp1.keyDown) && !(CheckCollisionRecs(flp2.body, lim.down.body))) //P2
                            flp2.body.y += FLIPPER_SPEED;
                        if (IsKeyDown(flp1.keyUp) && !(CheckCollisionRecs(flp2.body, lim.up.body)))
                            flp2.body.y -= FLIPPER_SPEED;

                        if (IsKeyDown(flp2.keyDown) && !(CheckCollisionRecs(flp1.body, lim.down.body))) //P1
                            flp1.body.y += FLIPPER_SPEED;
                        if (IsKeyDown(flp2.keyUp) && !(CheckCollisionRecs(flp1.body, lim.up.body)))
                            flp1.body.y -= FLIPPER_SPEED;

                        pong::game::flipperColCheck();
                        pong::game::limitColCheck();
                        pong::game::winCheck();

                        ball::ball.pos.x += BALL_SPEED * mod.speedVar;
                        ball::ball.pos.y += BALL_SPEED * 2 * mod.direction;

                    }
                    if(!mod.winState)
                        game::draw();

                }

            } while (!IsKeyPressed(KEY_F1) && !IsKeyPressed(KEY_ENTER) && !mod.winState );

        }

        void resetGeneralOptions() {
            //Pause state
            pre_config::pause = pre_config::DEFAULT_PAUSE;
            //WinState
            pre_config::mod.winState = false;
            //Scores
            pre_config::score.scoreP1 = 0;
            pre_config::score.scoreP2 = 0;
        }

    }

}