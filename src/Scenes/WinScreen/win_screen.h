#pragma once
#include <iostream>
#include <string>
#include "..\..\RayLib\include\raylib.h"
#include "Extern Vars/extern_var.h"

namespace pong {

    namespace win_screen {

        void draw(pong::Flipper);
        void winSign(pong::Flipper);

    }

}