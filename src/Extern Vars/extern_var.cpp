#include "extern_var.h"

namespace config {

	namespace pre_config {

		Modif mod;
		Scores score;
		bool quit;
		bool pause;

	}
	
	namespace ball {

		Ball ball;

	}

	namespace flipper {

		Flipper flp1;
		Flipper flp2;

	}

	namespace game_area {

		Limits lim;

	}

}