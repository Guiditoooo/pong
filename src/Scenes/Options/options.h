#pragma once
#include <iostream>
#include <string>
#include "..\..\RayLib\include\raylib.h"



namespace pong {

	namespace options {

		enum class OPTION { LIMIT_POINT, NAME_P1, NAME_P2, COLOR_P1, COLOR_P2, COLOR_BALL, COLOR_LIM, PU_P1, PU_P2, UP_P1, DOWN_P1, UP_P2, DOWN_P2, QUIT };
		const int OPT_Q = 14;

		extern int distance;
		extern int posXinit;
		extern int posYinit;

		void init();
		void draw();

	}

}